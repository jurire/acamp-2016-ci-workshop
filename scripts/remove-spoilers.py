#!/usr/bin/env python
import os
import sys
import argparse
import logging as log
from pprint import pformat, pprint

def get_options():
    parser = argparse.ArgumentParser(description='scraps the script from solutions')
    parser.add_argument('target', metavar='target', help='the filename')
    options = parser.parse_args()

    return options.target

if __name__ == '__main__':
    target = get_options()
    lines = open(target).readlines()
    result = []
    hiding = False
    for l in lines:
        if not hiding:
            if l.count('HIDE') > 0:
                hiding = True
        if hiding:
            if l.count('HIDE END') > 0:
                hiding = False
                continue
        if not hiding:
            result.append(l)
    print ''.join(result)


